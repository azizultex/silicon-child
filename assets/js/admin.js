jQuery(function($){

	$('#silicon-intro-app-showcase').css('display', 'none');
	var selected = $('.equip-image-select.active').data('value');
	if( selected != 'app-showcase' ) {
		$('.acf-postbox').css('display', 'none');
	}

	$('.equip-image-select').on('click', function(){
		if( $(this).data('value') == 'app-showcase') {
			$('.acf-postbox').css('display', 'block');
		} else {
			$('.acf-postbox').css('display', 'none');
		}
	});
});

