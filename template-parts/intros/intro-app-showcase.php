<?php

// Overriden template from parent theme to integrate ACF in the backend. 
$_intro_id = silicon_get_setting( 'intro', 0 );
if ( empty( $_intro_id ) ) {
    return;
}

$intro_tabs = get_field('intro_tabs', $_intro_id);
if(empty($intro_tabs)) {
    return;
}

?>
<script>
    window.cycleSecond = <?php the_field('tabs_speed', $_intro_id); ?>
</script>
<section class="intro-section intro-app-showcase intro-light">
    <div class="arrow bounce left"><span class="fa fa-arrow-left" aria-hidden="true"></span></div>
    <div class="arrow bounce right"><span class="fa fa-arrow-right" aria-hidden="true"></span></div>
    <?php
    // background
    $_is_first = true;
    foreach ( $intro_tabs as $key => $tab ) :
        // if ( empty( $tab['middle_image'] ) ) {
        //     continue;
        // }

        $_bg_atts = array();

        $_bg_atts['class']         = trim( 'intro-app-background ' . ( $_is_first ? 'active' : '' ) );
        $_bg_atts['data-platform'] = '#' . $tab['button_text'];

        if ( 'image' === $tab['background_type'] && ! empty( $tab['background_image'] ) ) {
            $_bg_atts['data-jarallax'] = true;
            $_bg_atts['style']         = silicon_css_background_image( (int) $tab['background_image'] );
        } else {
            $_bg_atts['style'] = silicon_css_background_color( sanitize_hex_color( $tab['background_color'] ) );
        }

        echo silicon_get_tag( 'div', $_bg_atts, '' );
        $_is_first = false;
    endforeach;
    unset( $_is_first, $key, $tab, $_bg_atts );
    ?>
    <div class="container padding-bottom-3x">
        <div class="row">
            <div class="col-md-12">
                <?php 
                    $_is_first = true;
                    foreach ( $intro_tabs as $key => $tab ) :
                ?>
                <div data-platform="#<?php echo esc_attr( $tab['button_text'] ); ?>" class="customized_title <?php echo ( $_is_first) ? 'active' : ''; ?>">
                    <h1 class="h1" style="text-align: center;font-size:<?php echo $tab['title_style']['font_size']; ?>px;color: <?php echo $tab['title_style']['text_color']; ?>">
                        <?php echo esc_html( trim( $tab['title'] ) ); ?>
                    </h1>
                    <div class="text-block padding-bottom-2x">
                        <p style="text-align: center;font-size:<?php echo $tab['subtitle_style']['font_size']; ?>px;color: <?php echo $tab['subtitle_style']['text_color']; ?>"><?php echo esc_html( trim( $tab['subtitle'] ) ); ?></p>
                    </div>
                </div>
                <?php 
                    $_is_first = false;
                    endforeach;
                    unset($key, $tab);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mobileTabs">
            </div>
            <div class="col-md-6">

                <?php
                // gadgets
                $_is_first = true;
                foreach ( $intro_tabs as $key => $tab ) :
                    // if ( empty( $_s['screen'] ) ) {
                    //     continue;
                    // }

                    echo '
                    <div class="custom-gadget gadget-' . $tab['button_text'] . ( $_is_first ? ' active' : '' ) . '" data-platform="#' . $tab['button_text'] . '">
                        <div class="screen">
                            ' . wp_get_attachment_image( (int) $tab['middle_image'], 'full' ) . '
                        </div>
                    </div>
                    ';

                    $_is_first = false;
                endforeach;
                unset( $_is_first, $key, $tab );
                ?>
            </div>
            <div class="col-md-6 relative">

                <?php
                // features
                $_f_tpl    = '<div class="feature">{icon}<div class="feature-text">{title}{desc}</div></div>';
                $_is_first = true;
                foreach ( $intro_tabs as $key => $tab ) :
                    // if ( empty( $_s['screen'] ) ) :
                    //     continue;
                    // endif;

                    ?>
                    <div class="intro-app-features <?php echo ( $_is_first ) ? 'active' : ''; ?>"
                         data-platform="#<?php echo esc_attr( $tab['button_text'] ); ?>">
                        <?php
                        // three features, increase $j for more features
                        for ( $_i = 0; $_i <= 2; $_i ++ ) :

                            $icon_type = $tab["features"][$_i]["icon_type"];
                            $icon_font = $tab["features"][$_i]["icon_font"];
                            $icon_font_style = $tab["features"][$_i]['icon_font_style'];
                            $icon_img = $tab["features"][$_i]["icon_img"];
                            $title = $tab["features"][$_i]["features_title"];
                            $desc = $tab["features"][$_i]["features_description"];

                            if ( empty( $icon ) && empty( $title ) && empty( $desc ) ) {
                                continue;
                            }

                            if ( $icon_type == 'icon_img' ) {
                                $icon = wp_get_attachment_image( (int) $icon_img, 'full' );
                                $icon = silicon_get_text( $icon, '<div class="feature-icon">', '</div>' );
                            } elseif ( $icon_type == 'icon_font') {
                                $icon = silicon_get_text( $icon_font, '<div class="feature-icon feature-icon-'.$key.'-'.$_i.'">', '</div>' );
                                echo "<style>
                                .feature-icon-{$key}-{$_i} i {
                                    font-size: {$icon_font_style['font_size']} !important;
                                    color: {$icon_font_style['text_color']} !important;
                                }
                                </style>";
                            } else {
                                $icon = '';
                            }

                            $r = array(
                                '{icon}'  => $icon,
                                '{title}' => silicon_get_text( esc_html( trim( $title ) ), '<h4 style="font-size:' . $tab['f_t_s']['font_size'] . 'px;color:' . $tab['f_t_s']['text_color'] . '">', '</h4>' ),
                                '{desc}'  => silicon_get_text( esc_html( trim( $desc ) ), '<p style="font-size:' . $tab['f_s_t_s']['font_size'] . 'px;color:' . $tab['f_s_t_s']['text_color'] . '">', '</p>' ),
                            );

                            echo str_replace( array_keys( $r ), array_values( $r ), $_f_tpl );
                            unset( $r, $icon );
                        endfor;
                        unset( $_i, $_j );
                        ?>
                    </div>
                    <?php

                    $_is_first = false;
                endforeach;
                unset( $_is_first, $_f_tpl );
                ?>
            </div>
            <div class="col-md-12 desktopTabs">
                <?php
                // headers
                $_h_tpl    = '<div class="{class}" data-platform="{platform}">{logo}{tagline}</div>';
                $_is_first = true;
                foreach ( $intro_tabs as $key => $tab ) :
                    // if ( empty( $_s['screen'] ) ) {
                    //     continue;
                    // }

                    if ( empty( $tab['icon'] ) ) {
                        $logo = '';
                    } else {
                        $logo = wp_get_attachment_image( (int) $tab['icon'], 'full', false, array(
                            'class' => 'block-center',
                            'alt'   => trim( strip_tags( get_post_meta( (int) $tab['icon'], '_wp_attachment_image_alt', true ) ) ),
                        ) );

                        $logo_meta  = wp_get_attachment_metadata( (int) $tab['icon'] );
                        $logo_width = empty( $logo_meta['width'] ) ? 240 : (int) $logo_meta['width'];

                        $logo = sprintf( '<div class="intro-app-logo" style="width: %2$dpx;">%1$s</div>',
                            $logo,
                            $logo_width / 2
                        );

                        unset( $logo_meta, $logo_width );
                    }

                    $r = array(
                        '{class}'    => esc_attr( trim( 'intro-app-header ' . ( $_is_first ? 'active' : '' ) ) ),
                        '{platform}' => '#' . $tab['button_text'],
                        '{logo}'     => $logo,
                        '{tagline}'  => silicon_get_text( esc_html( trim( $tab['tagline'] ) ), '<div class="intro-app-tagline" style="font-size:'. $tab['tagline_s']['font_size'] .'px; color:'. $tab['tagline_s']['text_color'].'">', '</div>' ),
                    );

                    echo str_replace( array_keys( $r ), array_values( $r ), $_h_tpl );
                    $_is_first = false;
                    unset( $r, $logo );
                endforeach;
                unset( $_h_tpl, $_is_first, $key, $tab );

                ?>
                <div class="platform-swith font-family-nav padding-top-1x">
                    <?php
                    // platform switcher
                    $_is_first = true;
                    foreach ( $intro_tabs as $key => $tab ) :
                        if ( empty( $tab['middle_image'] ) ) {
                            continue;
                        }

                        echo '
                        <a href="#' . $tab['button_text'] . '" class="platform-' . $tab['button_text'] . ( $_is_first ? ' active' : '' ) . '" style="font-size:' . $tab['buttons_s']['font_size'] . 'px;color:' . $tab['buttons_s']['text_color'] . ';background-color: '. $tab['btn_bg_color'] .'"> ' . $tab['button_icon'] .' ' . $tab['button_text'] . '
                        </a>
                        ';

                        $_is_first = false;
                    endforeach;
                    unset( $_is_first, $key, $tab );
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
