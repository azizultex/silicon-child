<?php
/**
 * Silicon Child
 *
 * @author 8guild
 */

/**
 * Enqueue child scripts and styles
 *
 * Note the priority: 20.
 * This function should be executed after the callback in the parent theme
 */
function silicon_child_assets() {
	wp_enqueue_style( 'silicon-child', get_stylesheet_directory_uri() . '/style.css', array(), null );
	wp_enqueue_script(  'touchSwipe', get_stylesheet_directory_uri().'/assets/js/jquery.touchSwipe.min.js', array('jquery'), null);
	wp_enqueue_script('particleground', get_stylesheet_directory_uri().'/assets/js/jquery.particleground.min.js', array('jquery'), null);
	wp_enqueue_script(  'child', get_stylesheet_directory_uri().'/assets/js/child.js', array('jquery'), null);
}
add_action( 'wp_enqueue_scripts', 'silicon_child_assets', 20 );

/**
 * Snippet Name: Add admin script on custom post types
 * Snippet URL: http://www.wpcustoms.net/snippets/add-admin-script-on-custom-post-types/
 */
 function wpc_add_admin_cpt_script( $hook ) {
    global $post;
    if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
        if ( 'silicon_intro' === $post->post_type ) {  
            wp_enqueue_script(  'custom_admin_script', get_stylesheet_directory_uri().'/assets/js/admin.js', array('jquery'), null);
        }
    }
}
add_action( 'admin_enqueue_scripts', 'wpc_add_admin_cpt_script', 10, 1 );

// custom author for blog grid
if(!function_exists('silicon_tile_footer')){
	function silicon_tile_footer(){
		$author = get_field('custom_author');
		$meta = silicon_get_entry_meta();
		echo '<footer class="post-footer font-family-nav">', $author, $meta, '</footer>';
	}
}
