jQuery(function($){

	function MobileAppShowCase(){
		this.cycleSecond = window.cycleSecond || 7000;
		this.mobileTabs = $('.mobileTabs');
		this.desktopTabs = $('.desktopTabs');
		this.platforms = $('.platform-swith a');
		this.tabs = this.desktopTabs.html();
		this.platformsLength = this.platforms.length;
		this.counter = 0;
		this.el = ['customized_title', 'intro-app-background', 'intro-app-header', 'platform-swith > a', 'custom-gadget', 'intro-app-features'];
	}

	MobileAppShowCase.prototype.loadTabs = function(){
		if($(window).width() > 991 ){
			this.mobileTabs.empty();
			this.desktopTabs.html(this.tabs);
		} else {
			this.desktopTabs.empty();
			this.mobileTabs.html(this.tabs);
		}
	}	

	MobileAppShowCase.prototype.cycle = function(){
		var index = this.counter;
		$.each(this.el, function(i, value){
			$('.' + value ).removeClass('active');
			$('.' + value ).eq(index).addClass('active');
		});
	}

	var mobileAppShowCase = new MobileAppShowCase();

	// load tabs 
	mobileAppShowCase.loadTabs();

	// load tabs on window resize
	$(window).on('resize', function(){
		mobileAppShowCase.loadTabs();
	});

	// initial cycling
	timer = setInterval(function(){
		mobileAppShowCase.cycle(); 
		mobileAppShowCase.counter += 1; // move for next tab
		mobileAppShowCase.counter = mobileAppShowCase.counter ===mobileAppShowCase.platformsLength ? 0 : mobileAppShowCase.counter;
	}, mobileAppShowCase.cycleSecond);

	// stop tabs cycling on click
	$('.platform-swith > a').live('click', function(e){
		clearInterval(timer);
		mobileAppShowCase.counter = $(this).index();
		mobileAppShowCase.cycle();
	});

	// enable swiping 
	$('.custom-gadget, .intro-app-features').swipe({
		swipeLeft: function(){
			clearInterval(timer);
			mobileAppShowCase.counter = mobileAppShowCase.platformsLength === mobileAppShowCase.counter + 1 ? 0 : mobileAppShowCase.counter + 1;
			mobileAppShowCase.cycle();
		},
		swipeRight: function(){
			clearInterval(timer);
			mobileAppShowCase.counter =  mobileAppShowCase.counter === 0 ? mobileAppShowCase.platformsLength - 1 : mobileAppShowCase.counter - 1;
			mobileAppShowCase.cycle();
		}
	});

	// enable arrow button works
	$('.arrow.left').on('click', function(){
		clearInterval(timer);
		mobileAppShowCase.counter =  mobileAppShowCase.counter === 0 ? mobileAppShowCase.platformsLength - 1 : mobileAppShowCase.counter - 1;
		mobileAppShowCase.cycle();
	});	

	$('.arrow.right').on('click', function(){
		clearInterval(timer);
		mobileAppShowCase.counter = mobileAppShowCase.platformsLength === mobileAppShowCase.counter + 1 ? 0 : mobileAppShowCase.counter + 1;
		mobileAppShowCase.cycle();
	});


	// Particle Background
	$('#particles').particleground({
		dotColor: '#970ef6',
		lineColor: '#970ef6'
	});

		/**
	 * Smooth scroll for the partner page button
	 */
	$(function() {
	  $('.partner-smooth, .partner-smooth>a').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        $('html,body').animate({
	          scrollTop: target.offset().top - 0
	        }, 1000);
	        return false;
	      }
	    }
	  });	

	});	

});